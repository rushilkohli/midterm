package inventory;

import java.util.Scanner;

/**
 * Simulator to add items, print inventory and to search item's quantity.
 *
 * @author Rushil Kohli
 * @author Paul Bonenfant
 */
public class InventorySystem extends Simulation {

    public static void main(String[] args) {
        
        simulation();

    }
    
}
